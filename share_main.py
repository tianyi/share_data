# -*- coding: utf-8 -*-
import pandas
import tushare as ts
from datetime import datetime, timedelta
import os


# 默认拿多少天的数据
DEFAULT_DAYS_DATA = 30


def get_next_day(date_obj, is_next=True, days=1):
    if is_next:
        next_day = date_obj + timedelta(days=days)
    else:
        next_day = date_obj - timedelta(days=days)
    return next_day


def get_next_day_from_str(date_str, is_next=True):
    date_obj = get_datetime_from_str(date_str)
    next_day = get_next_day(date_obj, is_next)
    str_1 = get_str_form_datetime(next_day)
    return str_1


def get_offset_day_from_str(date_str, offset=1, is_next=True):
    date_obj = get_datetime_from_str(date_str)
    next_day = get_next_day(date_obj, is_next, days=offset)
    str_1 = get_str_form_datetime(next_day)
    return str_1


def get_str_form_datetime(date_obj):
    return date_obj.strftime("%Y-%m-%d")


def get_datetime_from_str(date_str):
    m_date = datetime.strptime(date_str, '%Y-%m-%d')
    return m_date


def get_day_data_share(share_code, date_str):
    df = ts.get_k_data(
        share_code,
        start=date_str,
        end=date_str,
        ktype="D",
    )
    return df


def get_date(share_code, date_str, is_next=True):
    '''获取交易数据 知道获取成功, 如果没有数据则试图获取15次'''
    date_str_now = date_str
    df = None
    for obj in range(15):
        df = get_day_data_share(share_code, date_str_now)
        if df.empty:
            if is_next:
                date_str_now = get_next_day_from_str(date_str_now, is_next)
        else:
            break
    return (df, date_str_now)


def get_csc_line(ts_result):
    xx = ts_result.values
    line_0 = ",".join(
        [
            str(xx[0][6]), str(xx[0][0]), str(xx[0][1]), 
            str(xx[0][2]), str(xx[0][3]), str(xx[0][4]), str(xx[0][5])
        ]
    )
    return line_0


if __name__ == '__main__':
    # pfe = pandas.read_excel('test1.xlsx')
    filename = datetime.now().strftime("%Y-%m-%dT%H%M%S") + ".csv"
    # rows = pfe.shape[0] -1
    # import pdb; pdb.set_trace()
    startrow = 0
    with open("test.csv", "r") as input_file:
        for line in input_file:
            share_name = line[0]
            share_code = line[1]
            share_init_date = line[2]
            before_day = line[3]
            after_day = line[4]
            # 计算下一日期
            print("%s-%s-%s-%s-%s" % (share_name, share_code, share_init_date, before_day, after_day))
            # 确定财报日: 输入日期拿不到数据, 往后找, 直到拿到数据, 这一天作为财报日
            date_0 = share_init_date
            df_0 = get_date(share_code, date_0, is_next=True)
            
            date_str_1 = get_next_day_from_str(date_0, is_next=True)
            date_1 = get_date(share_code, date_str_1, is_next=True)
            if date_1[0].empty:
                raise NotImplementedError("循环了15次还没有结果")
            else:
                df_1 = date_1[0]

            date_str_2 = get_next_day_from_str(date_1[1], is_next=True)

            date_2 = get_date(share_code, date_str_2, is_next=True)
            if date_2[0].empty:
                raise NotImplementedError("循环了15次还没有结果")
            else:
                df_2 = date_2[0]

            date_m1 = get_next_day_from_str(date_0, is_next=False)
            print("获取上一个日期=%s" % date_m1)
            data_m1 = get_date(share_code, date_m1, is_next=False)
            if data_m1[0].empty:
                raise NotImplementedError(" data_m1")
            else:
                df_m1 = data_m1[0]

            date_m2 = get_next_day_from_str(data_m1[1], is_next=False)
            print("获取上一个日期=%s" % date_m2)
            data_m2 = get_date(share_code, date_m2, is_next=False)
            if data_m2[0].empty:
                raise NotImplementedError(" data_m2")
            else:
                df_m2 = data_m2[0]
            startcol = 6
            # df_0.values[0][0]: 日期
            # df_0.values[0][1]: open
            # df_0.values[0][2]: close
            # df_0.values[0][3]: high
            # df_0.values[0][4]: low
            # df_0.values[0][5]: volume
            # df_0.values[0][6]: code
            line_0 = get_csc_line(df_0)
            print(line_0)
            line_1 = get_csc_line(df_1)
            print(line_1)
            line_2 = get_csc_line(df_2)
            print(line_2)
            line_m1 = get_csc_line(df_m1)
            print(line_m1)
            line_m2 = get_csc_line(df_m2)
            print(line_m2)
            all_line = ",".join([line_m2, line_m1, line_0, line_1, line_2])
            all_line = share_name + "," + all_line
            print(all_line)
            # if not os.path.exists(filename):
            with open(filename, "a") as myfile:
                myfile.write(all_line)
                myfile.write("\n")