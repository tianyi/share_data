# -*- coding: utf-8 -*-
# import pandas
import tushare as ts
from datetime import datetime, timedelta
# import os
import time


# 默认拿多少天的数据
DEFAULT_DAYS_DATA = 30


def get_offset_date(date_obj, offset, is_next):
    '''
    date_obj: 日期对象
    offset: 日期加减的天数
    is_next: True 往后, False 往前
    '''
    if is_next:
        next_day = date_obj + timedelta(days=offset)
    else:
        next_day = date_obj - timedelta(days=offset)
    return next_day


def convert_datetime_to_str(date_obj):
    return date_obj.strftime("%Y-%m-%d")


def convert_str_to_datetime(date_str):
    return datetime.strptime(date_str, '%Y-%m-%d')


def data_to_line(data_row, share_code):
    '''把 pandas.core.frame.DataFrame 行数据输入为line'''
    if data_row is not None:
        date_list = [
            str(data_row.code),
            str(data_row.date),
            str(data_row.open),
            str(data_row.close),
            str(data_row.high),
            str(data_row.low),
            str(data_row.volume),
        ]
    else:
        date_list = [share_code, "NA", 'NA', 'NA', 'NA', 'NA', 'NA']
    line = ",".join(date_list)
    return line


def extract_data(share_code, share_init_date, before_day, after_day):
    result = {
        "df_obj": None,
        "start_index": 0,
        "end_index": 0,
        "start_date_str": "",
        "end_date_str": "",
    }
    init_date_obj = convert_str_to_datetime(share_init_date)
    # 计算开始日期
    start_date_obj = get_offset_date(
        date_obj=init_date_obj,
        offset=DEFAULT_DAYS_DATA,
        is_next=False)
    # 计算结束日期
    end_date_obj = get_offset_date(
        date_obj=init_date_obj,
        offset=DEFAULT_DAYS_DATA,
        is_next=True)
    start_date_str = convert_datetime_to_str(start_date_obj)
    end_date_str = convert_datetime_to_str(end_date_obj)
    result["start_date_str"] = start_date_str
    result["end_date_str"] = end_date_str
    print("%s--%s--%s--%s" % (
        share_code, share_init_date, start_date_str, end_date_str))
    # 后去股票数据
    df = ts.get_k_data(
        share_code,
        start=start_date_str,
        end=end_date_str,
        ktype="D",
    )
    if df.empty:
        # import pdb; pdb.set_trace()
        print("get data from tushare error. share_code=%s, start=%s, end=%s" %
              (share_code, start_date_str, end_date_str))
        return result
    # 确定 当前日期锚点: 从给定时间往后找, 找到第一天有数据的为止
    # 查询给定的日期
    init_day_data = df.loc[df['date'] == share_init_date]
    # 是否查找到初始化的数据日期
    is_found_init_data = False
    if init_day_data.empty:
        # 给定的日期没有数据: 循环确定锚点日期
        for index in range(1, DEFAULT_DAYS_DATA - after_day):
            share_init_date_obj = convert_str_to_datetime(share_init_date)
            # 往后一天
            next_date_obj = get_offset_date(share_init_date_obj, index, True)
            next_date_str = convert_datetime_to_str(next_date_obj)
            init_day_data = df.loc[df['date'] == next_date_str]
            if not init_day_data.empty:
                is_found_init_data = True
                break
            else:
                # 不是这个, 进入下一次循环
                continue
    else:
        is_found_init_data = True

    if is_found_init_data:
        # 这里利用的 tushare 给的那个 index数据
        init_index = init_day_data.iloc[0].name
        start_index = init_index - before_day
        end_index = init_index + after_day
        result['start_index'] = start_index
        result['end_index'] = end_index
        result["df_obj"] = df
    return result


if __name__ == '__main__':
    start_time = time.time()
    # print("start time=%s" % start_time)
    filename = datetime.now().strftime("%Y-%m-%dT%H%M%S") + ".csv"
    '''
    result = {
        "df_obj": None,
        "start_index": 0,
        "end_index": 0,
        "start_date_str": "",
        "end_date_str": "",
    }
    '''
    with open("test1.csv", "r") as input_file:
        for line in input_file:
            line = line.replace("\n", "")
            line_list = line.split(",")
            # import pdb; pdb.set_trace()
            share_name = line_list[0]
            share_code = line_list[1]
            # print(share_code)
            share_init_date = line_list[2]
            before_day = int(line_list[3])
            after_day = int(line_list[4])
            # import pdb; pdb.set_trace()
            result = extract_data(share_code, share_init_date, before_day, after_day)
            print("%s--%s--%s--%s--%s" % (
                share_name,
                share_code,
                share_init_date,
                result["start_date_str"],
                result["end_date_str"])
            )
            # 保存数据
            start_index = result["start_index"]
            end_index = result["end_index"]
            df = result["df_obj"]
            if df is not None:
                line_data = []
                # if share_code == "600212":
                #     import pdb; pdb.set_trace()
                while start_index <= end_index:
                    # <class 'pandas.core.series.Series'>
                    try:
                        data_row = df.loc[start_index]
                    except Exception as ex:
                        print(ex)
                        data_row = None
                    line = data_to_line(data_row, share_code)
                    line_data.append(line)
                    start_index += 1
                # 写入文件
                all_line = ",".join(line_data)
                with open(filename, "a") as myfile:
                    myfile.write(all_line)
                    myfile.write("\n")
            else:
                # 处理异常
                # import pdb; pdb.set_trace()
                # 写去对应的空数据列数
                line = data_to_line(data_row, share_code)
                all_count = before_day + after_day + 1
                start_count = 1
                line_data = []
                while start_count <= all_count:
                    line = data_to_line(None, share_code)
                    line_data.append(line)
                    start_count += 1
                all_line = ",".join(line_data)
                with open(filename, "a") as myfile:
                    myfile.write(all_line)
                    myfile.write("\n")
                print("error: share_code=%s" % share_code)

    end_time = time.time()
    print("start time=%s" % end_time)
    print("took: %2.4f sec" % (start_time - end_time))
